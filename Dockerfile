FROM python:3.8-slim-buster as builder

ARG TERRAFORM_VER=0.12.24
ARG PACKER_VER=1.5.5

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends jq unzip wget curl file binutils && \
    mkdir ~/bin
# create user
RUN useradd --create-home --user-group -c "builder user" appuser
WORKDIR /home/appuser
USER appuser
ENV PATH=$PATH:~/.local/bin
# install Ansible
RUN pip install --user "ansible>=2.9,<2.10"
# install Terraform
RUN wget -nv -O /tmp/terraform.zip https://releases.hashicorp.com/terraform/$TERRAFORM_VER/terraform_${TERRAFORM_VER}_linux_amd64.zip && \
    unzip /tmp/terraform.zip -d ~/.local/bin/
# install Packer
RUN wget -nv -O /tmp/packer.zip https://releases.hashicorp.com/packer/$PACKER_VER/packer_${PACKER_VER}_linux_amd64.zip && \
    unzip /tmp/packer.zip -d ~/.local/bin/ && \
    strip ~/.local/bin/packer
# install AWS CLI v2
RUN wget -nv -O /tmp/awscliv2.zip https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip && \
    unzip /tmp/awscliv2.zip && \
    ./aws/install -i ~/.local/lib/ -b ~/.local/bin
